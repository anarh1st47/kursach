#include "DB_lib.h"

static inline void isOk(bool ok, struct table& el, bool isDelete){
  if(isDelete){
    ok = !ok;
	  if(!ok)        el.name = '\0', el.author = '\0', el.genre = '\0', el.year = '\0', el.page = '\0';
	}
	else        el.isPrint = ok;
}

//Проверяет, не равен ли элемент NULL, если равен, то заменяем его
void checkNull(char *&elem){
  if(elem == '\0')        elem = (char*) "NULL";
  return;
}

//Запускает проверку всех элементов
void checkAll(struct table &el){
  checkNull(el.name);
  checkNull(el.genre);
  checkNull(el.author);
  checkNull(el.page);
  checkNull(el.year);
  return;
}



// SELECT WHERE
void selectTables(char* text, struct table items[], int &numberOfStrings, bool isDelete){
  
  char *query = strtok(text, " "), *tmp = strtok (NULL, " "), *tmp2 = strtok (NULL, " ");
  
  
  for(int i = 0; i < numberOfStrings; i++){
    if(!strcmp(query, "year"))
      if(!strcmp(tmp, "<"))
	      isOk(strcmp(items[i].year, tmp2) < 0, items[i], isDelete);
      else if(!strcmp(tmp, ">"))
	      isOk(strcmp(items[i].year, tmp2) > 0, items[i], isDelete);
      else if(!strcmp(tmp, "="))
	      isOk(!strcmp(items[i].year, tmp2), items[i], isDelete);
	    else if(!strcmp(tmp, "!="))
	      isOk(strcmp(items[i].year, tmp2), items[i], isDelete); 
      else if(!strcmp(tmp, "<="))
	      isOk(strcmp(items[i].year, tmp2) <= 0, items[i], isDelete);
      else if(!strcmp(tmp, ">="))
	      isOk(strcmp(items[i].year, tmp2) >= 0, items[i], isDelete); 
	  
	  
	  if(!strcmp(query, "author"))
	    if(!strcmp(tmp, "="))
	      isOk(!strcmp(items[i].author, tmp2), items[i], isDelete);
	  if(!strcmp(query, "genre"))
	    if(!strcmp(tmp, "="))
	      isOk(!strcmp(items[i].genre, tmp2), items[i], isDelete);
	  if(!strcmp(query, "name"))
	    if(!strcmp(tmp, "="))
	      isOk(!strcmp(items[i].name, tmp2), items[i], isDelete);
	  if(!strcmp(query, "page"))
	    if(!strcmp(tmp, "="))
	      isOk(!strcmp(items[i].page, tmp2), items[i], isDelete);
	      
	}
  return;
}

// Print line
void hr(bool genre, bool name, bool author, bool page, bool year, bool first){
  if(first) printf("\n");
  if(name)
    printf("+---------");
  if(author)
    printf("+------------");
  if(genre)
    printf("+--------------");
  if(page)
    printf("+---------");
  if(year)
    printf("+---------");
  printf("+\n");
  return;
}

//Print all table
void printTable(struct table items[], int numberOfStruct, bool name, bool author, bool genre, bool page, bool year){
  char *null = (char*) "NULL";
  if(name+author+genre+page+year != 0){
    hr(genre, name, author, page, year, 1);
    if(name)
      printf("|   name  ");
    if(author)
      printf("|   author   ");
    if(genre)
      printf("|    genre     ");
    if(page)
      printf("|   page  ");
    if(year)
      printf("|   year  ");
    printf("|");
    hr(genre, name, author, page, year, 1);

    for(int j = 0; j < numberOfStruct; j++){
      checkAll (items[j]); // без этого ошибка сегментации
      if(!items[j].isPrint) continue;
      //printf("%i", strcmp(items[j].name, "NULL"));
      if( strcmp(items[j].name, "NULL") || strcmp(items[j].author, "NULL") || strcmp(items[j].genre, "NULL") || strcmp(items[j].page, "NULL") || strcmp(items[j].year, "NULL") )
      //if( items[j].name && items[j].author && items[j].genre && items[j].page && items[j].year )
      {
        printf("|");
        if(name)
          printf("%9s|",  strcmp(items[j].name, "NULL") ? items[j].name : " ");
        if(author)
          printf("%12s|", strcmp(items[j].author, "NULL") ? items[j].author : " ");
        if(genre)
          printf("%14s|", strcmp(items[j].genre, "NULL") ? items[j].genre : " ");
        if(page)
          printf("%9s|",  strcmp(items[j].page, "NULL") ? items[j].page : " ");
        if(year)
          printf("%9s|",  strcmp(items[j].year, "NULL") ? items[j].year : " ");
        //hr(genre, name, author, page, year, 1);
        printf("\n");
      }
    }
    hr(genre, name, author, page, year, 0);
  }
  return;
}

//SELECT ORDER BY
void orderTables(char* text, struct table items[]){
  printf(text);
  char *table = text;
  if(!strcmp(table, "genre"))
    qs_struct_genre(items, 0, 2);
  else if(!strcmp(table, "name"))
    qs_struct_name(items, 0, 2);
  else if(!strcmp(table, "author"))
    qs_struct_author(items, 0, 2);
  else if(!strcmp(table, "page"))
    qs_struct_page(items, 0, 2);
  else if(!strcmp(table, "year"))
    qs_struct_year(items, 0, 2);
}
