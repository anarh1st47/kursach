#include "table.c"

void *qs_struct_genre(struct table items[], int left, int right){ //Бытсрая сортировка структур
  int i, j;
  char *x = items[(left+right)/2].genre;
  struct table temp;
  i = left; j = right;

  do{
    while( (strcmp(items[i].genre, x) < 0) && (i < right)) i++; 
    while( (strcmp(items[j].genre, x) > 0) && (j > left) ) j--; 
    if(i <= j){
      temp = items[i];
      items[i] = items[j];
      items[j] = temp;
      i++; j--;
    }
  } while(i <= j);

  if(left < j)    qs_struct_genre(items, left, j);
  if(i < right)   qs_struct_genre(items, i, right);
}

void *qs_struct_name(struct table items[], int left, int right){ //Бытсрая сортировка структур
  int i, j;
  char *x = items[(left+right)/2].name;
  struct table temp;
  i = left; j = right;

  do{
    while( (strcmp(items[i].name, x) < 0) && (i < right)) i++; 
    while( (strcmp(items[j].name, x) > 0) && (j > left) ) j--; 
    if(i <= j){
      temp = items[i];
      items[i] = items[j];
      items[j] = temp;
      i++; j--;
    }
  } while(i <= j);

  if(left < j)    qs_struct_name(items, left, j);
  if(i < right)   qs_struct_name(items, i, right);
}

void *qs_struct_author(struct table items[], int left, int right){ //Бытсрая сортировка структур
  int i, j;
  char *x = items[(left+right)/2].author;
  struct table temp;
  i = left; j = right;

  do{
    while( (strcmp(items[i].author, x) < 0) && (i < right)) i++; 
    while( (strcmp(items[j].author, x) > 0) && (j > left) ) j--; 
    if(i <= j){
      temp = items[i];
      items[i] = items[j];
      items[j] = temp;
      i++; j--;
    }
  } while(i <= j);

  if(left < j)    qs_struct_author(items, left, j);
  if(i < right)   qs_struct_author(items, i, right);
}

void *qs_struct_page(struct table items[], int left, int right){ //Бытсрая сортировка структур
  int i, j;
  char *x = items[(left+right)/2].page;
  struct table temp;
  i = left; j = right;

  do{
    while( (strcmp(items[i].page, x) < 0) && (i < right)) i++; 
    while( (strcmp(items[j].page, x) > 0) && (j > left) ) j--; 
    if(i <= j){
      temp = items[i];
      items[i] = items[j];
      items[j] = temp;
      i++; j--;
    }
  } while(i <= j);

  if(left < j)    qs_struct_page(items, left, j);
  if(i < right)   qs_struct_page(items, i, right);
}

void *qs_struct_year(struct table items[], int left, int right){ //Бытсрая сортировка структур
  int i, j;
  char *x = items[(left+right)/2].year;
  struct table temp;
  i = left; j = right;

  do{
    while( (strcmp(items[i].year, x) < 0) && (i < right)) i++; 
    while( (strcmp(items[j].year, x) > 0) && (j > left) ) j--; 
    if(i <= j){
      temp = items[i];
      items[i] = items[j];
      items[j] = temp;
      i++; j--;
    }
  } while(i <= j);

  if(left < j)    qs_struct_year(items, left, j);
  if(i < right)   qs_struct_year(items, i, right);
}
