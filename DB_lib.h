#ifndef _DBLIB_H
#define _DBLIB_H
void selectTables(char*, struct table[], int&, bool);
void orderTables (char*, struct table[]);
void printTable(struct table[], int, bool = 1, bool = 1, bool = 1, bool = 1, bool = 1);
void hr(bool, bool, bool, bool, bool, bool);
void checkNull(char*&);
void checkAll(struct table&);
#endif
